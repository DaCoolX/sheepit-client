package com.sheepit.client.datamodel;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

@NoArgsConstructor @Root(strict = false, name = "job") @Getter @ToString public class RenderTask {
	
	@Attribute(name = "id") private String id;
	
	@Attribute(name = "use_gpu") private int useGpu;
	
	@ElementList(name = "chunks") private List<Chunk> chunks;
	
	@Attribute(name = "path") private String path;
	
	@Attribute(name = "frame") private String frame;
	
	@Attribute(name = "synchronous_upload") private String synchronousUpload;
	
	@Attribute(name = "validation_url") private String validationUrl;
	
	@Attribute(name = "name") private String name;
	
	@Attribute(name = "password") private char[] password;
	
	@Element(name = "renderer") private RendererInfos rendererInfos;
	
	@Element(name = "script", data = true) private String script;
	
}
